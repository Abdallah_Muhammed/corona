
package com.example.corona.model.covid_by_country;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("lastChecked")
    @Expose
    private String lastChecked;
    @SerializedName("covid19Stats")
    @Expose
    private List<Covid19Stat> covid19Stats = null;

    public String getLastChecked() {
        return lastChecked;
    }

    public void setLastChecked(String lastChecked) {
        this.lastChecked = lastChecked;
    }

    public List<Covid19Stat> getCovid19Stats() {
        return covid19Stats;
    }

    public void setCovid19Stats(List<Covid19Stat> covid19Stats) {
        this.covid19Stats = covid19Stats;
    }

}
