package com.example.corona.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static Retrofit retrofit ;

    public static Retrofit getClient(String base_url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create(gson))

                    .build();

        return retrofit;
    }
}
