package com.example.corona.api;

import com.example.corona.model.covid_all.ResponseCovidAll;
import com.example.corona.model.covid_by_country.ResponseCovidByCountry;
import com.example.corona.model.news.ResponseNews;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Api {


    @GET("stats")
    Call<ResponseCovidByCountry> getStatComvid(@Header("x-rapidapi-host")String host , @Header("x-rapidapi-key") String key,
                                               @Query("country")String country);

    @GET("api")
    Call<ResponseCovidAll> getAllStatComvid(@Header("x-rapidapi-host")String host , @Header("x-rapidapi-key") String key);



    @GET("top-headlines")
    Call<ResponseNews> getNews(@Query("apiKey")String apiKey ,@Query("country")String country,@Query("category")String category);


}
