package com.example.corona.ui.statements;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.adapter.StatementAdapter;
import com.example.corona.databinding.StatementesFragmentBinding;
import com.example.corona.model.StatementsModel;
import com.example.corona.model.news.ResponseNews;

import java.util.List;

public class StatementesFragment extends Fragment {

    private StatementesViewModel mViewModel;
    private StatementesFragmentBinding binding ;
    private StatementAdapter adapter ;

    public static StatementesFragment newInstance() {
        return new StatementesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         binding = DataBindingUtil.inflate(inflater,R.layout.statementes_fragment, container, false);

         binding.setLifecycleOwner(this);
         return binding.getRoot();

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(StatementesViewModel.class);
        mViewModel.getData(getContext(),"Eg","Health").observe(getViewLifecycleOwner(), new Observer<ResponseNews>() {
            @Override
            public void onChanged(ResponseNews responseNews) {
                binding.progStat.setVisibility(View.GONE);
                adapter = new StatementAdapter();
                binding.reStatements.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.reStatements.setAdapter(adapter);
                adapter.setModels(responseNews.getArticles());
            }
        });
    }

}
