package com.example.corona.ui.show_image;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.example.corona.R;
import com.example.corona.databinding.ActivityShowImageBinding;
import com.example.corona.helper.GlideApp;

public class ShowImageActivity extends AppCompatActivity {

    private ActivityShowImageBinding binding ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_show_image);


        String image = getIntent().getStringExtra("image");

        GlideApp.with(this).load(image)
                .into(binding.imgShow);
        binding.closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
