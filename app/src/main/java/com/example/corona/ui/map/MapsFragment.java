package com.example.corona.ui.map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.databinding.FragmentMapsBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsFragment extends Fragment {

    private OnMapReadyCallback callback = new OnMapReadyCallback() {


        @Override
        public void onMapReady(GoogleMap googleMap) {


            LatLng city = getLocationFromAddress(getContext(), name);
            googleMap.addMarker(new MarkerOptions().position(city).title(name));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(city));

        }
    };
    private double mLat;
    private double mLong;
    private String name ,number;
    private FragmentMapsBinding binding ;
    private String deaths ,recovered;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_maps, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }

         mLat = getArguments().getDouble("lat");
         mLong = getArguments().getDouble("long");
         name = getArguments().getString("name");
         number = getArguments().getString("number");

        recovered = getArguments().getString("recovered");
        deaths = getArguments().getString("deaths");
        binding.recovered.setText(recovered);
        binding.deaths.setText(deaths);
        binding.confirmed.setText(number);
        binding.cityName.setText(name);
        binding.statNumber.setText(number);
    }



    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = new LatLng(mLat,mLong);

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null ) {
                return null;
            }else {

                if (address.size()>0){
                    Address location = address.get(0);
                    p1 = new LatLng(location.getLatitude(), location.getLongitude() );
                }

            }



        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }
}
