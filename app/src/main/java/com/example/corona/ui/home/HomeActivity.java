package com.example.corona.ui.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.corona.R;
import com.example.corona.ui.AboutCoronaFragment;
import com.example.corona.ui.AboutUsFragment;
import com.example.corona.ui.home.ui.home.HomeFragment;
import com.example.corona.ui.language.LanguageFragment;
import com.example.corona.ui.notifications.NotificationActivity;
import com.example.corona.ui.statements.StatementesFragment;
import com.example.corona.ui.statistics.StatisticsFragment;
import com.example.corona.ui.videos.VideosFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import static com.example.corona.helper.HelperMethod.replace;
import static com.example.corona.helper.PlayerConfig.URL_APP_SHARE;
import static com.example.corona.helper.PlayerConfig.URL_MINISTRY_OF_HEALTHE;
import static com.example.corona.helper.PlayerConfig.URL_WORLD_HEALTH;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, NavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView navigation ;
    private Toolbar toolbar;
    private TextView tolbar_title ;
    private ImageView  notificationView;
    private long backTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
         toolbar = findViewById(R.id.toolbar_home);
        navigation = findViewById(R.id.navigation);
        tolbar_title = findViewById(R.id.tolbar_title);
        notificationView = findViewById(R.id.icon_noti);
        setSupportActionBar(toolbar);
        tolbar_title.setText(getString(R.string.menu_home));

        replace(new HomeFragment(),R.id.home_container,getSupportFragmentManager().beginTransaction());

        navigation.setOnNavigationItemSelectedListener(this);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        notificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(), NotificationActivity.class));

            }
        });


    }




    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id._nv_home:
                replace(new HomeFragment(),R.id.home_container,getSupportFragmentManager().beginTransaction(),getString(R.string.tag_home));
                tolbar_title.setText(getString(R.string.menu_home));
                break;

            case R.id.nv_win:
                replace(new StatisticsFragment(),R.id.home_container,getSupportFragmentManager().beginTransaction(),getString(R.string.tag_win));
                tolbar_title.setText(getString(R.string.num));

                break;

            case R.id.nv_videos:


                replace(new VideosFragment(),R.id.home_container,getSupportFragmentManager().beginTransaction(),getString(R.string.tag_video));
                tolbar_title.setText(getString(R.string.videos));

                break;




                // تصريحات المسؤوليين
            case R.id.nv_more:

                replace(new StatementesFragment(),R.id.home_container,getSupportFragmentManager().beginTransaction(),getString(R.string.tag_stat));
                tolbar_title.setText(getString(R.string.Congig));


                break;

            case R.id.ministry_of_health_nav:

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_MINISTRY_OF_HEALTHE)));

                break;

            case R.id.world_health_organization_nav:

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_WORLD_HEALTH)));

                break;


            case R.id.about_corona_virus_nav:

                replace(new AboutCoronaFragment() , R.id.home_container,getSupportFragmentManager().beginTransaction(),getString(R.string.tag_abou_v));
                tolbar_title.setText(getString(R.string.about_corona_virus));


                break;




                case R.id.notification_nav:

                    startActivity(new Intent(getApplicationContext(),NotificationActivity.class));
                break;


            case R.id.language_nav:

                replace(new LanguageFragment(),R.id.home_container,getSupportFragmentManager().beginTransaction(),getString(R.string.tag_language));
                tolbar_title.setText(getString(R.string.choose_the_language));

                break;case R.id.about_us_nav:

                replace(new AboutUsFragment(),R.id.home_container,getSupportFragmentManager().beginTransaction(),getString(R.string.tag_about_us));
                tolbar_title.setText(getString(R.string.about_us));

                break;

            case R.id.share_app_nav :

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        URL_APP_SHARE);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        VideosFragment videosFragment = (VideosFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_video));
        LanguageFragment  languageFragment = (LanguageFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_language));
        StatementesFragment  statementesFragment = (StatementesFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_stat));
        HomeFragment  fragmenthome = (HomeFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_home));
        StatisticsFragment statisticsFragment = (StatisticsFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_win));

        AboutCoronaFragment  aboutCoronaFragment = (AboutCoronaFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_abou_v));
        AboutUsFragment  aboutUsFragment = (AboutUsFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_about_us));



        if (videosFragment != null && videosFragment.isVisible()) {

        }else  if (statementesFragment !=null&& statementesFragment.isVisible()){

        }

else  if (aboutUsFragment !=null&& aboutUsFragment.isVisible()){

 }

else  if (aboutCoronaFragment !=null&& aboutCoronaFragment.isVisible()){

 }

else  if (languageFragment !=null&& languageFragment.isVisible()){


        }else  if (fragmenthome !=null&& fragmenthome.isVisible()){

            if (backTime + 3000 >System.currentTimeMillis()){

                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }else {

                Toast.makeText(this, R.string.press_again, Toast.LENGTH_SHORT).show();
            }
            backTime = System.currentTimeMillis();


        }else  if (statisticsFragment !=null&& statisticsFragment.isVisible()){



        }else {



            super.onBackPressed();

        }


    }
}

