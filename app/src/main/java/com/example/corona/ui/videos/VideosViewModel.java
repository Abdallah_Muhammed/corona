package com.example.corona.ui.videos;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.corona.R;
import com.example.corona.model.NotificationModel;
import com.example.corona.model.VideosModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class VideosViewModel extends ViewModel {

    MutableLiveData<List<VideosModel>> data = new
            MutableLiveData<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("videos");

    public MutableLiveData<List<VideosModel>> getData(Context context) {
        final List<VideosModel> list = new ArrayList<>();


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                for (DataSnapshot snapshot:dataSnapshot.getChildren()
                ) {


                    VideosModel value = snapshot.getValue(VideosModel.class);

                    list.add(value);



                }

                data.setValue(list);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });



        return data;
    }
}
