package com.example.corona.ui.statements.details;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.corona.R;
import com.example.corona.databinding.FragmentDetailsStatementsBinding;
import com.example.corona.databinding.StatementesFragmentBinding;
import com.example.corona.helper.GlideApp;
import com.example.corona.ui.show_image.ShowImageFragment;

import static com.example.corona.helper.HelperMethod.replace;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsStatementsFragment extends Fragment {

    private FragmentDetailsStatementsBinding binding ;
    public DetailsStatementsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_details_statements, container, false);

        ImageView statImage = binding.statImage;

        Bundle arguments = getArguments();
        String big_title = arguments.getString("big_title");
        String date = arguments.getString("date");
        String desc = arguments.getString("desc");
        final String image = arguments.getString("image");
        final String site = arguments.getString("site");


        binding.siteDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(site)));

            }
        });

        GlideApp.with(this).load(image).into(statImage);


        binding.bigTitle.setText(big_title);
        binding.statDate.setText(date);
        if (desc != null){
            binding.detailsStatement.setText(desc);

        }else {
            binding.detailsStatement.setText("");

        }

        statImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShowImageFragment fragment = new ShowImageFragment();
                Bundle  bundle = new Bundle();

                bundle.putString("image",image);

                fragment.setArguments(bundle);
                replace(fragment,R.id.home_container,getActivity().getSupportFragmentManager().beginTransaction(),"show_imag");

            }
        });
        return binding.getRoot();
    }







}



