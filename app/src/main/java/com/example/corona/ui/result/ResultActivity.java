package com.example.corona.ui.result;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.example.corona.R;
import com.example.corona.databinding.ActivityResultBinding;
import com.example.corona.ui.home.HomeActivity;

public class ResultActivity extends AppCompatActivity {

    private ActivityResultBinding binding ;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_result);

        int result = getIntent().getIntExtra("result", 10);

        binding.resultProg.setProgress(result*12);

        binding.tvResult.setText(""+result);


        if (result==0){

            binding.statusResult.setText(getString(R.string.if_symptoms_are_apparent_n_please_consult_your_nearest_soctor));

        }else if (result<=4){

            binding.statusResult.setText(getString(R.string.zero_to_4));
        }else if (result<=6){
            binding.statusResult.setText(getString(R.string.foure_to_6));


        }else if (result<=8){



            binding.statusResult.setText(getString(R.string.more_than_6));

        }

        binding.backHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ResultActivity.this, HomeActivity.class));
                finish();
            }
        });
   }
}
