package com.example.corona.ui.statistics;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.adapter.StatisticsAdapter;
import com.example.corona.databinding.FragmentStatisticsBinding;
import com.example.corona.model.StatistcsModel;
import com.example.corona.model.covid_all.ResponseCovidAll;
import com.example.corona.model.covid_by_country.ResponseCovidByCountry;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticsFragment extends Fragment {

    private FragmentStatisticsBinding binding ;
    private StatisticsViewModel viewModel;
    private StatisticsAdapter adapter ;

    public StatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate
                (inflater,R.layout.fragment_statistics, container, false);

        binding.setLifecycleOwner(this);
        viewModel = ViewModelProviders.of(this).get(StatisticsViewModel.class);

        getdata(viewModel);
        search();
        return binding.getRoot();
    }

    private void search() {



        binding.search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                adapter.getFilter().filter(charSequence);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void getdata(StatisticsViewModel viewModel) {

        viewModel.getData(getContext()).observe(getViewLifecycleOwner(), new Observer<ResponseCovidAll>() {
            @Override
            public void onChanged(ResponseCovidAll statistcsModels) {

                binding.progStatit.setVisibility(View.GONE);
                binding.search.setVisibility(View.VISIBLE);
                adapter = new StatisticsAdapter();
                binding.rvStatistics.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.rvStatistics.setAdapter(adapter);
                adapter.setModels(statistcsModels.getCountriesStat());
            }
        });

    }
}
