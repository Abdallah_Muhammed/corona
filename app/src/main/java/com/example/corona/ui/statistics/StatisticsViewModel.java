package com.example.corona.ui.statistics;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.corona.R;
import com.example.corona.model.StatistcsModel;
import com.example.corona.model.covid_all.ResponseCovidAll;
import com.example.corona.model.covid_by_country.ResponseCovidByCountry;
import com.example.corona.repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class StatisticsViewModel extends ViewModel {


    MutableLiveData<ResponseCovidAll> data ;

    Repository repository ;
    public StatisticsViewModel() {
        data = new MutableLiveData<>();
        repository = Repository.getInstance();
    }

    public MutableLiveData<ResponseCovidAll> getData(Context context) {

        repository.getAllStatus().enqueue(new Callback<ResponseCovidAll>() {
            @Override
            public void onResponse(Call<ResponseCovidAll> call, Response<ResponseCovidAll> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ResponseCovidAll> call, Throwable t) {

            }
        });


        return data;
    }
}
