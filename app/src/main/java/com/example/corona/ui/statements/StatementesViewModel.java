package com.example.corona.ui.statements;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.corona.R;
import com.example.corona.model.StatementsModel;
import com.example.corona.model.covid_by_country.ResponseCovidByCountry;
import com.example.corona.model.news.ResponseNews;
import com.example.corona.repository.Repository;

import java.util.ArrayList;
import java.util.List;


public class StatementesViewModel extends ViewModel {

    Repository repository ;


    public StatementesViewModel() {

        repository  = Repository.getInstance();
    }

    MutableLiveData<ResponseNews> data = new MutableLiveData<>();

    public MutableLiveData<ResponseNews> getData(final Context context, String country , String q) {


        repository.getNews(country,q).enqueue(new Callback<ResponseNews>() {
            @Override
            public void onResponse(Call<ResponseNews> call, Response<ResponseNews> response) {


                if (response.body().getStatus().equals("ok")) {
                    data.setValue(response.body());


                }else {


                    Toast.makeText(context, ""+response.body().getStatus(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseNews> call, Throwable t) {


            }
        });




        return data;
    }
}
