package com.example.corona.ui.protection;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.corona.R;
import com.example.corona.adapter.SelectionAdapter;
import com.example.corona.databinding.ActivityProtectionVirusBinding;

import java.util.Arrays;

public class ProtectionVirusActivity extends AppCompatActivity {


    private SelectionAdapter selectionAdapter;

    private ActivityProtectionVirusBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind = DataBindingUtil.setContentView(this,R.layout.activity_protection_virus);


        initSmpVirus();
        // toolbar

        setSupportActionBar(bind.toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.protection_virus));


        }

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }



    private void initSmpVirus() {

        selectionAdapter = new SelectionAdapter(1);
        bind.rvImageProtection.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));
        bind.rvImageProtection.setAdapter(selectionAdapter);
        selectionAdapter.setList(Arrays.asList(
                "https://www.skynewsarabia.com/images/v1/2020/03/15/1328425/2000/1125/1-1328425.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584024284_402_79121_arwaystopreventthespreadofinfections3942c6316c074bea8a3d17675b51fdc3.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584022596_233_38242_1.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584023445_510_18894_2.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584023537_325_24955_3.jpg",
                "https://www.moh.gov.sa/HealthAwareness/EducationalContent/Corona/PublishingImages/001.png",
                "https://www.moh.gov.sa/HealthAwareness/EducationalContent/Corona/PublishingImages/004.png"
        ));
    }
}
