package com.example.corona.ui.home.ui.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.corona.R;
import com.example.corona.adapter.SelectionAdapter;
import com.example.corona.databinding.FragmentHomeBinding;
import com.example.corona.helper.GlideApp;
import com.example.corona.helper.PrefManager;
import com.example.corona.model.covid_by_country.Covid19Stat;
import com.example.corona.model.covid_by_country.ResponseCovidByCountry;
import com.example.corona.ui.protection.ProtectionVirusActivity;
import com.example.corona.ui.quiz.QuizActivity;
import com.example.corona.ui.symptoms_virus.SymptomsVirusActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class HomeFragment extends Fragment implements View.OnClickListener {


    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // if current fragment is not the last one goto next fragment
            // if current fragment is the last one goto first fragment
            if (bind.viewPager.getCurrentItem() != sliderAdapter.getItemCount() - 1) {
                bind.viewPager.setCurrentItem(bind.viewPager.getCurrentItem() + 1);
            } else {
                bind.viewPager.setCurrentItem(0);
            }
            handler.postDelayed(runnable, slideDuration);
        }
    };
    //get time when touching slider
    private long timeAtTouch = 0;

    private HomeViewModel homeViewModel;

    private final long slideDuration = 3000;
    private final Handler handler = new Handler();

    private FragmentHomeBinding bind;
    private HomeSliderAdapter sliderAdapter;
    private SelectionAdapter selectionAdapter;
    private PrefManager  manager ;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        bind.setLifecycleOwner(this);

        initSlider();
        initSmpVirus();
        initProtection();

        bind.showAll.setOnClickListener(this);
        bind.showAllProtection.setOnClickListener(this);




        return bind.getRoot();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        manager = new PrefManager(getContext());

//        String country = manager.getCountry();



//
//        if (country.equals("Egypt")|country.equals("egypt")|country.contains("eg")|country.contains("EG")|country.contains("Eg")){
//
//            // set flag icon egypt
//
//            Drawable img = getContext().getResources().getDrawable(R.drawable.ic_egypt);
//            img.setBounds(0, 0, 60, 60);
//            bind.last.setCompoundDrawables(null, null, img, null);
//
//
//        }else{
//
//            Drawable img = getContext().getResources().getDrawable(R.drawable.ic_saudi_arabia);
//            img.setBounds(0, 0, 60, 60);
//            bind.last.setCompoundDrawables(null, null, img, null);
//
//
//        }


        homeViewModel.getStatus("Egypt").observe(getViewLifecycleOwner(), new Observer<ResponseCovidByCountry>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChanged(ResponseCovidByCountry responseCovidByCountry) {


//
                    bind.convInjures.setText(responseCovidByCountry.getData().getCovid19Stats().get(0).getConfirmed()+"");
                    bind.convRecoveres.setText(responseCovidByCountry.getData().getCovid19Stats().get(0).getRecovered()+"");
                    bind.convDeaths.setText(responseCovidByCountry.getData().getCovid19Stats().get(0).getDeaths()+"");
                String[] ts = responseCovidByCountry.getData().getLastChecked().split("T");
                bind.dateLast.setText(ts[0]);


            }
        });


    }

    private void initProtection() {

        selectionAdapter = new SelectionAdapter(0);
        bind.rvProtectionVirus.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        bind.rvProtectionVirus.setAdapter(selectionAdapter);
        selectionAdapter.setList(Arrays.asList(
                "https://www.skynewsarabia.com/images/v1/2020/03/15/1328425/2000/1125/1-1328425.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584024284_402_79121_arwaystopreventthespreadofinfections3942c6316c074bea8a3d17675b51fdc3.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584022596_233_38242_1.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584023445_510_18894_2.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584023537_325_24955_3.jpg",
                "https://www.moh.gov.sa/HealthAwareness/EducationalContent/Corona/PublishingImages/001.png",
                "https://www.moh.gov.sa/HealthAwareness/EducationalContent/Corona/PublishingImages/004.png"

        ));
    }

    private void initSmpVirus() {

        selectionAdapter = new SelectionAdapter(0);
        bind.rvSympVirus.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        bind.rvSympVirus.setAdapter(selectionAdapter);
        selectionAdapter.setList(Arrays.asList(
                "https://www.skynewsarabia.com/web/images/v1/2020/03/15/1328421/1100/619/1-1328421.jpg"
                ,
                "https://www.skynewsarabia.com/images/v1/2020/03/15/1328424/2000/1125/1-1328424.jpg"
                ,"https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584021950_864_44119_.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584020933_690_91081__111046208_corona_virus_symptoms_short_v2_arabic_640nc.png"
                ,"https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584021411_442_108867_67e15e5f821cd2e74db593aff0f21437.jpeg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584022240_949_52678_.jpg"

                ,"https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584022240_949_52678_.jpg",
                "https://www.moh.gov.sa/HealthAwareness/EducationalContent/Corona/PublishingImages/003.png",
                "https://cdn.al-ain.com/lg/images/2020/1/22/78-201916-infograph-corona-virus-syndrome-infection-2.jpeg"


        ));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.show_all:

                startActivity(new Intent(getContext(), SymptomsVirusActivity.class));

                break;


            case R.id.show_all_protection:
                startActivity(new Intent(getContext(), ProtectionVirusActivity.class));


                break;

        }

    }


    private class HomeSliderAdapter extends RecyclerView.Adapter<HomeSliderAdapter.VH> {

        private List<String> list = new ArrayList<>();

        public List<String> getList() {
            return list;
        }

        public void setList(List<String> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slider, parent, false), list);
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position) {
            String url = list.get(position);

            GlideApp.with(getContext()).load(url).into(holder.imageView);

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(getContext(), QuizActivity.class));
                }
            });

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class VH extends RecyclerView.ViewHolder {
            ImageView imageView;

            VH(@NonNull View itemView, final List<String> list) {
                super(itemView);

                imageView = itemView.findViewById(R.id.imageView);
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, slideDuration);
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }


    private void initSlider() {
        sliderAdapter = new HomeSliderAdapter();
        bind.viewPager.setAdapter(sliderAdapter);
        bind.indicator.setViewPager2(bind.viewPager);
        //stop slider from auto sliding when user touches the slider
        //same idea as whatsApp status
        bind.viewPager.getChildAt(0).setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //save time at touch
                    timeAtTouch = System.currentTimeMillis();

                    //stop sliding
                    handler.removeCallbacks(runnable);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //calculate passed time
                    long passed = System.currentTimeMillis() - timeAtTouch;
                    long diff = slideDuration - passed;

                    // if passed time longer than specified duration then slide at once
                    // else wait for difference
                    if (diff <= 0) {
                        //continue sliding
                        handler.postDelayed(runnable, 0);
                    } else {
                        //continue sliding
                        handler.postDelayed(runnable, diff);
                    }
                }
                return false;
            }
        });



        sliderAdapter.setList(Arrays.asList(
                "https://mostaql.hsoubcdn.com/uploads/39122-1428397385-0d3f444e62f103.png",
                "https://mostaql.hsoubcdn.com/uploads/39122-1428397385-0d3f444e62f103.png",
                "https://mostaql.hsoubcdn.com/uploads/39122-1428397385-0d3f444e62f103.png"
        ));
    }


}
