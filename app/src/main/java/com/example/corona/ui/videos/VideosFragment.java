package com.example.corona.ui.videos;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.adapter.VideosAdapter;
import com.example.corona.databinding.VideosFragmentBinding;
import com.example.corona.model.VideosModel;

import java.util.List;

public class VideosFragment extends Fragment {

    private VideosViewModel mViewModel;
    private VideosFragmentBinding binding ;
    private VideosAdapter adapter;

    public static VideosFragment newInstance() {
        return new VideosFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,R.layout.videos_fragment, container, false);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(VideosViewModel.class);
        mViewModel.getData(getContext()).observe(getViewLifecycleOwner(), new Observer<List<VideosModel>>() {
            @Override
            public void onChanged(List<VideosModel> videosModels) {
                binding.progVideo.setVisibility(View.GONE);
                binding.rvVideos.setLayoutManager(new LinearLayoutManager(getContext()));
                adapter = new VideosAdapter();
                binding.rvVideos.setAdapter(adapter);
                adapter.setVideosModels(videosModels);
            }
        });


    }

}
