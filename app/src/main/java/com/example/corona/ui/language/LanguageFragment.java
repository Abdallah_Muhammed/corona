package com.example.corona.ui.language;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.databinding.FragmentLanguageBinding;
import com.example.corona.helper.PrefManager;
import com.example.corona.ui.home.ui.home.HomeFragment;

import java.util.Locale;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import static com.example.corona.helper.HelperMethod.replace;

/**
 * A simple {@link Fragment} subclass.
 */
public class LanguageFragment extends Fragment implements View.OnClickListener {

    private FragmentLanguageBinding binding ;
    private PrefManager manager;
    private String language;

    public LanguageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_language,container,false);

//         check if the choose language is arabic or not
        manager = new PrefManager(getContext());
        String language = manager.getLanguage();
        if (language.equals("en")){

            binding.arabicLanguage.setTextColor(Color.GRAY);
            binding.englishLanguage.setTextColor(Color.WHITE);
            binding.arabicLanguage.setBackgroundResource(R.drawable.shape_no_rectangle);
            binding.englishLanguage.setBackgroundResource(R.drawable.shap_search);

        }else {

            binding.englishLanguage.setBackgroundResource(R.drawable.shape_no_rectangle);
            binding.arabicLanguage.setBackgroundResource(R.drawable.shap_search);
            binding.arabicLanguage.setTextColor(Color.WHITE);
            binding.englishLanguage.setTextColor(Color.GRAY);

        }

        binding.arabicLanguage.setOnClickListener(this);
        binding.englishLanguage.setOnClickListener(this);
        binding.doit.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.arabic_language:
                binding.englishLanguage.setBackgroundResource(R.drawable.shape_no_rectangle);
                binding.arabicLanguage.setBackgroundResource(R.drawable.shap_search);
                binding.arabicLanguage.setTextColor(Color.WHITE);
                binding.englishLanguage.setTextColor(Color.GRAY);

                language = "ar";

                break;


            case R.id.english_language:
                binding.arabicLanguage.setTextColor(Color.GRAY);
                binding.englishLanguage.setTextColor(Color.WHITE);
                binding.arabicLanguage.setBackgroundResource(R.drawable.shape_no_rectangle);
                binding.englishLanguage.setBackgroundResource(R.drawable.shap_search);
                language = "en";


                break;


            case R.id.doit:

                if (language!=null){

                    setlanguage(language);
                    refreshTheApp();
                }

                break;
        }
    }


    private void setlanguage(String yourLanguage){

        manager.saveLanguage(yourLanguage);

        Locale locale = new Locale(yourLanguage);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getResources().updateConfiguration(config,
                getActivity().getResources().getDisplayMetrics());


    }

    private void refreshTheApp(){

        Intent intent = getActivity().getIntent();
        getActivity().finish();
        startActivity(intent);
    }
}
