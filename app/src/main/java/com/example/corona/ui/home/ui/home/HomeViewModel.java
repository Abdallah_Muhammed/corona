package com.example.corona.ui.home.ui.home;

import com.example.corona.model.covid_by_country.ResponseCovidByCountry;
import com.example.corona.repository.Repository;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {


    Repository repository ;
    private MutableLiveData<ResponseCovidByCountry> data ;
    public HomeViewModel() {

        repository = Repository.getInstance();
        data = new MutableLiveData<>();
    }





    public MutableLiveData<ResponseCovidByCountry> getStatus(String country){

        repository.getStatus(country).enqueue(new Callback<ResponseCovidByCountry>() {
            @Override
            public void onResponse(Call<ResponseCovidByCountry> call, Response<ResponseCovidByCountry> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ResponseCovidByCountry> call, Throwable t) {

            }
        });


        return data;


    }




}