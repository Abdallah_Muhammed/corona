package com.example.corona.ui.notifications;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.corona.R;
import com.example.corona.adapter.NotificationAdapter;
import com.example.corona.databinding.ActivityNotificationBinding;
import com.example.corona.model.NotificationModel;

import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    private ActivityNotificationBinding binding;
    private NotificationViewModel mViewmodel;
    private NotificationAdapter adapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        mViewmodel = ViewModelProviders.of(this).get(NotificationViewModel.class);

        setSupportActionBar(binding.toolbar);


        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.notifications));


            mViewmodel.getData(this).observe(this, new Observer<List<NotificationModel>>() {
                @Override
                public void onChanged(List<NotificationModel> notificationModels) {

                    adapter = new NotificationAdapter();
                    binding.rvNoti.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    binding.rvNoti.setAdapter(adapter);
                    adapter.setList(notificationModels);
                }
            });
        }




    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
