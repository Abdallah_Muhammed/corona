package com.example.corona.ui.symptoms_virus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.corona.R;
import com.example.corona.adapter.SelectionAdapter;
import com.example.corona.databinding.ActivitySymptomsVirusBinding;

import java.util.Arrays;

public class SymptomsVirusActivity extends AppCompatActivity {

    private SelectionAdapter selectionAdapter;
    private ActivitySymptomsVirusBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bind = DataBindingUtil.setContentView(this,R.layout.activity_symptoms_virus);

        initSmpVirus();
        // toolbar

        setSupportActionBar(bind.toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.symptoms_virus));


        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }



    private void initSmpVirus() {

        SelectionAdapter    selectionAdapter = new SelectionAdapter(1);
        bind.rvImageSymp.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));
        bind.rvImageSymp.setAdapter(selectionAdapter);
        selectionAdapter.setList(Arrays.asList(
                "https://www.skynewsarabia.com/web/images/v1/2020/03/15/1328421/1100/619/1-1328421.jpg"
                ,
                "https://www.skynewsarabia.com/images/v1/2020/03/15/1328424/2000/1125/1-1328424.jpg"
                ,"https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584021950_864_44119_.jpg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584020933_690_91081__111046208_corona_virus_symptoms_short_v2_arabic_640nc.png"
                ,"https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584021411_442_108867_67e15e5f821cd2e74db593aff0f21437.jpeg",
                "https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584022240_949_52678_.jpg"

                ,"https://magrabiwebsite.s3-accelerate.amazonaws.com/2020/03/1584022240_949_52678_.jpg",
                "https://www.moh.gov.sa/HealthAwareness/EducationalContent/Corona/PublishingImages/003.png",
                "https://cdn.al-ain.com/lg/images/2020/1/22/78-201916-infograph-corona-virus-syndrome-infection-2.jpeg"
        ));
    }
}
