 package com.example.corona.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import com.example.corona.R;
import com.example.corona.helper.PrefManager;
import com.example.corona.ui.home.HomeActivity;

import java.util.Locale;

 public class SplashActivity extends AppCompatActivity {

     Thread splashTread;
     PrefManager manager ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

         manager = new PrefManager(getApplicationContext());
        Locale locale = new Locale(manager.getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config,
                getResources().getDisplayMetrics());




        String country = getApplicationContext().getResources().getConfiguration().locale.getDisplayCountry();

        if (country.equals("مصر")){

            country = "Egypt";
        }else if (country.contains("ألسعودية")){

            country = "Saudi Arabia";
        }else if (country.equals("Saudi")){

            country = "Saudi Arabia";
        }


        manager.setCountry(country);

        startSplash();
    }


     private void startSplash() {


         splashTread = new Thread() {
             @Override
             public void run() {
                 try {
                     int waited = 0;
                     // Splash screen pause time
                     while (waited < 6000) {
                         sleep(50);
                         waited += 100;
                     }
                     Intent intent = new Intent(SplashActivity.this,
                             HomeActivity.class);
                     intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                     startActivity(intent);
                     SplashActivity.this.finish();
                 } catch (InterruptedException e) {
                     // do nothing
                 } finally {
                     SplashActivity.this.finish();
                 }

             }
         };
         splashTread.start();
     }

 }
