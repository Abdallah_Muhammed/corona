package com.example.corona.ui;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.databinding.FragmentAboutCoronaBinding;


public class AboutCoronaFragment extends Fragment {

    private FragmentAboutCoronaBinding binding ;
    public AboutCoronaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_about_corona, container, false);


        return binding.getRoot();
    }
}
