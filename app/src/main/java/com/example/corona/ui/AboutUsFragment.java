package com.example.corona.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.databinding.FragmentAboutCoronaBinding;
import com.example.corona.databinding.FragmentAboutUsRagmentBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {

    private FragmentAboutUsRagmentBinding binding;
    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_about_us_ragment, container, false);
        // Inflate the layout for this fragment

        binding.siteMbn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mahercp.net")));

            }
        });
        return binding.getRoot();
    }
}
