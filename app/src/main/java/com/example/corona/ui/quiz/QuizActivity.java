package com.example.corona.ui.quiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.corona.R;
import com.example.corona.databinding.ActivityQuizBinding;
import com.example.corona.ui.result.ResultActivity;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityQuizBinding binding ;
  private   String questions[] ;


    String answers[] ;

    String opt[] ;

    int flag=0;
    public static int marks,correct,wrong;

    private String myAnswer = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_quiz);
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setTitle(R.string.simple_test);
        questions = new String[]{
                getString(R.string.q_one),
                getString(R.string.q_two),
                getString(R.string.q_three),
                getString(R.string.q_four),
                getString(R.string.q_five),
               getString(R.string.q_six),
                getString(R.string.q_siven),
                getString(R.string.q_eight),


        };



        answers = new String[] {getString(R.string.yes),
                getString(R.string.yes),
                getString(R.string.yes),
                getString(R.string.yes),
               getString(R.string.yes),
                getString(R.string.yes),
                getString(R.string.yes),
                getString(R.string.yes)};



        opt =  new String[] {
                getString(R.string.yes),getString(R.string.no),
                getString(R.string.yes),getString(R.string.no),
                getString(R.string.yes),getString(R.string.no),
                 getString(R.string.yes),getString(R.string.no),
                getString(R.string.yes),getString(R.string.no),
                 getString(R.string.yes),getString(R.string.no),
                  getString(R.string.yes),getString(R.string.no),
                getString(R.string.yes),getString(R.string.no),


        };
        marks = 0;
        correct =0;
        wrong= 0;
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.simple_test));


        }
        binding.yes.setOnClickListener(this);
        binding.no.setOnClickListener(this);
        binding.next.setOnClickListener(this);
        binding.tvque.setText(questions[flag]);
        binding.yes.setText(opt[0]);
        binding.no.setText(opt[1]);

        binding.positionQuiz.setText(""+questions.length+"/"+flag);


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.yes:

                binding.yes.setBackgroundResource(R.drawable.shap_search);
                binding.yes.setTextColor(Color.WHITE);
                binding.no.setTextColor(Color.GRAY);
                binding.no.setBackgroundResource(R.drawable.shape_no_rectangle);
                binding.next.setVisibility(View.VISIBLE);
                myAnswer = binding.yes.getText().toString();

                break;

            case R.id.no:
                binding.no.setBackgroundResource(R.drawable.shap_search);
                binding.no.setTextColor(Color.WHITE);
                binding.yes.setTextColor(Color.GRAY);

                binding.yes.setBackgroundResource(R.drawable.shape_no_rectangle);
                binding.next.setVisibility(View.VISIBLE);
                myAnswer = binding.no.getText().toString();

                break;

            case R.id.next:



                if (myAnswer.equals(answers[flag])){

                    correct++;

                }else {
                    wrong++;
                }

                flag++;

                binding.positionQuiz.setText(""+questions.length+"/"+flag);

                if (flag<questions.length){
                    binding.tvque.setText(questions[flag]);
                    binding.yes.setText(opt[flag*2]);
                    binding.no.setText(opt[flag*2 +1]);

                }else {


                    marks = correct;

                    Intent intent = new Intent(this, ResultActivity.class);
                    intent.putExtra("result",marks);
                    startActivity(intent);
                    finish();
                }





                break;
        }
    }
}
