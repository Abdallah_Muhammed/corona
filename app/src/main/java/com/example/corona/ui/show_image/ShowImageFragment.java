package com.example.corona.ui.show_image;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.databinding.FragmentShowImageBinding;
import com.example.corona.helper.GlideApp;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShowImageFragment extends Fragment {

    private FragmentShowImageBinding binding;
    public ShowImageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_show_image, container, false);

        String image = null;
        if (getArguments() != null) {
            image = getArguments().getString("image");
        }
        GlideApp.with(getContext()).load(image)
                .into(binding.imgShow);
        binding.closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return binding.getRoot();
    }
}
