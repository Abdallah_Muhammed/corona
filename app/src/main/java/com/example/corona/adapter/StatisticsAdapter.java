package com.example.corona.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.corona.R;
import com.example.corona.databinding.ItemStatisticsBinding;
import com.example.corona.databinding.ItemVidrosBinding;
import com.example.corona.helper.GlideApp;
import com.example.corona.helper.PrefManager;
import com.example.corona.model.StatistcsModel;
import com.example.corona.model.VideosModel;
import com.example.corona.model.covid_all.CountriesStat;
import com.example.corona.model.covid_by_country.Covid19Stat;
import com.example.corona.model.covid_by_country.ResponseCovidByCountry;
import com.example.corona.ui.map.MapsFragment;

import java.util.ArrayList;
import java.util.List;

import static com.example.corona.helper.HelperMethod.replace;

public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.VH>  implements Filterable {

     List<CountriesStat> models = new ArrayList<>();
     List<CountriesStat> modelsFull = new ArrayList<>();
    Context context;
    PrefManager manager ;

    @NonNull
    @Override
    public StatisticsAdapter.VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();
        manager = new PrefManager(context);
        ItemStatisticsBinding binding  = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_statistics,parent,false);
        return new VH(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull StatisticsAdapter.VH holder, int position) {

        holder.binding.setStatistics(models.get(position));
        final CountriesStat countriesStat = models.get(position);


        holder.binding.statParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MapsFragment mapsFragment = new MapsFragment();
                Bundle bundle = new Bundle();

                if (manager.getCountry().equals("Egypt")){
                    bundle.putDouble("lat",26.8206);
                    bundle.putDouble("long",30.8025);

                }else{

                    bundle.putDouble("lat",23.8859);
                    bundle.putDouble("long",45.0792);
                }



                bundle.putString("name",countriesStat.getCountryName());
                bundle.putString("number",countriesStat.getCases());
                bundle.putString("deaths",countriesStat.getDeaths());
                bundle.putString("recovered",countriesStat.getTotalRecovered());
                mapsFragment.setArguments(bundle);
                replace(mapsFragment,R.id.home_container,((FragmentActivity)context).getSupportFragmentManager().beginTransaction(),"map");


            }
        });

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void setModels(List<CountriesStat> models){

        this.models= models;
        modelsFull = new ArrayList<>(models);
    }

    @Override
    public Filter getFilter() {


        return modelsFilter;
    }


    private Filter modelsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {


            List<CountriesStat>  filteredModels = new ArrayList<>();

            if (charSequence ==null || charSequence.length() ==0){

                filteredModels.addAll(modelsFull);
            }else{

                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (CountriesStat item :
                        modelsFull) {

                    if (item.getCountryName().toLowerCase().contains(filterPattern)){

                        filteredModels.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();

            results.values = filteredModels;
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {


            models.clear();
            models.addAll((List)filterResults.values);
            notifyDataSetChanged();
        }
    };

    public class VH extends RecyclerView.ViewHolder {
        ItemStatisticsBinding binding ;
        public VH(@NonNull ItemStatisticsBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }
}
