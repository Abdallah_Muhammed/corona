package com.example.corona.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.corona.R;
import com.example.corona.databinding.ItemNotoficationsBinding;
import com.example.corona.databinding.ItemVidrosBinding;
import com.example.corona.helper.GlideApp;
import com.example.corona.model.NotificationModel;
import com.example.corona.model.VideosModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.VH> {

    List<NotificationModel>  list = new ArrayList<>();

    @NonNull
    @Override
    public NotificationAdapter.VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemNotoficationsBinding binding  = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_notofications,parent,false);
        return new VH(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.VH holder, int position) {

        holder.binding.setNoti(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<NotificationModel>  list){

        this.list= list;

        notifyDataSetChanged();
    }

    public class VH extends RecyclerView.ViewHolder {
        ItemNotoficationsBinding binding ;
        public VH(@NonNull ItemNotoficationsBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }
}
