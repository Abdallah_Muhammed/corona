package com.example.corona.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.corona.R;
import com.example.corona.databinding.ItemVidrosBinding;
import com.example.corona.helper.GlideApp;
import com.example.corona.model.VideosModel;

import java.util.ArrayList;
import java.util.List;


public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.VH> {

    List<VideosModel> videosModels = new ArrayList<>();

    Context context ;
    @NonNull
    @Override
    public VideosAdapter.VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();
        ItemVidrosBinding binding  = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_vidros,parent,false);
        return new VH(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VideosAdapter.VH holder, final int position) {

        holder.binding.setVideos(videosModels.get(position));
        GlideApp.with(holder.itemView).load(videosModels.get(position).getImage()).into(holder.binding.imageVideos);



        holder.binding.imageVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+videosModels.get(position).getUrl())));

            }
        });



    }

    @Override
    public int getItemCount() {
        return videosModels.size();
    }

    public void setVideosModels(List<VideosModel> videosModels){

        this.videosModels= videosModels;
    }

    public class VH extends RecyclerView.ViewHolder {
        ItemVidrosBinding binding ;
        public VH(@NonNull ItemVidrosBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }
}
