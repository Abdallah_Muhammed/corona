package com.example.corona.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.corona.R;
import com.example.corona.helper.GlideApp;

import java.util.ArrayList;
import java.util.List;

public class HomeSliderAdapter extends RecyclerView.Adapter<HomeSliderAdapter.VH> {

    private List<String> list = new ArrayList<>();

    public List<String> getList() {
        return list;
    }

    Context context ;

    public void setList(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public HomeSliderAdapter.VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new HomeSliderAdapter.VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slider, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull HomeSliderAdapter.VH holder, int position) {

        String url=list.get(position);
        GlideApp.with(context).load(url).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VH extends RecyclerView.ViewHolder {

        ImageView imageView;

        public VH(@NonNull View itemView) {
            super(itemView);

            imageView=itemView.findViewById(R.id.imageView);
        }
    }
}
