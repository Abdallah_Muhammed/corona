package com.example.corona.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.corona.R;
import com.example.corona.databinding.ItemStatementsBinding;
import com.example.corona.helper.GlideApp;
import com.example.corona.model.StatementsModel;
import com.example.corona.model.news.Article;
import com.example.corona.ui.statements.details.DetailsStatementsFragment;

import java.util.ArrayList;
import java.util.List;

import static com.example.corona.helper.HelperMethod.replace;

public class StatementAdapter extends RecyclerView.Adapter<StatementAdapter.VH> {

    List<Article>  models = new ArrayList<>();
    Context context ;

    @NonNull
    @Override
    public StatementAdapter.VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();
        ItemStatementsBinding binding  = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_statements,parent,false);
        return new VH(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull StatementAdapter.VH holder, int position) {

        holder.binding.setStat(models.get(position));

        GlideApp.with(holder.itemView).load(models.get(position).getUrlToImage()).into(holder.binding.statImage);
        Article article = models.get(position);

        String[] ts = article.getPublishedAt().split("T");
        holder.binding.statDate.setText(ts[0]);
        final DetailsStatementsFragment fragment = new DetailsStatementsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("big_title",article.getTitle());
        bundle.putString("date",ts[0]);
        bundle.putString("desc", String.valueOf(article.getDescription()));
        bundle.putString("image",article.getUrlToImage());
        bundle.putString("site",article.getUrl());

        fragment.setArguments(bundle);

        holder.binding.itemParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replace(fragment,R.id.home_container,((FragmentActivity)context).getSupportFragmentManager().beginTransaction(),"stat");

            }
        });

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void setModels(List<Article> models){

        this.models= models;
    }

    public class VH extends RecyclerView.ViewHolder {
        ItemStatementsBinding binding ;
        public VH(@NonNull ItemStatementsBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }
}
