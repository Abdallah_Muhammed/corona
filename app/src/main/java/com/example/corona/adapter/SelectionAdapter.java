package com.example.corona.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.corona.R;
import com.example.corona.helper.GlideApp;
import com.example.corona.ui.home.ui.home.HomeFragment;
import com.example.corona.ui.show_image.ShowImageActivity;
import com.example.corona.ui.show_image.ShowImageFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.example.corona.helper.HelperMethod.replace;

public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.VH> {

    private List<String> list = new ArrayList<>();

    private int id ;
    public SelectionAdapter(int id ) {

        this.id = id;

    }

    public List<String> getList() {
        return list;
    }

    private Context context;

    public void setList(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final VH holder, int position) {
        final String url=list.get(position);
        GlideApp.with(context).load(url)
                .into(holder.imageView);



            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {





                        ActivityOptionsCompat compat = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            compat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,holder.imageView, Objects.requireNonNull(ViewCompat.getTransitionName(holder.imageView)));
                        }

                        Intent intent = new Intent(context, ShowImageActivity.class);
                        intent.putExtra("image",url);

                        context.startActivity(intent,compat.toBundle());

                    }

            });




    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        ImageView imageView;

        VH(@NonNull View itemView) {
            super(itemView);

            imageView=itemView.findViewById(R.id.imageView_select);
        }
    }
}
