package com.example.corona.repository;

import com.example.corona.api.Api;
import com.example.corona.model.covid_all.ResponseCovidAll;
import com.example.corona.model.covid_by_country.ResponseCovidByCountry;
import com.example.corona.model.news.ResponseNews;


import retrofit2.Call;

import static com.example.corona.api.ApiClient.getClient;
import static com.example.corona.helper.PlayerConfig.API_KEY_NEWS;
import static com.example.corona.helper.PlayerConfig.COMVID_ALL_BASE_URL;
import static com.example.corona.helper.PlayerConfig.COMVID_BASE_URL;
import static com.example.corona.helper.PlayerConfig.HOST_COVID_ALL;
import static com.example.corona.helper.PlayerConfig.HOST_COVID_COUNTRY;
import static com.example.corona.helper.PlayerConfig.KEY_COVID_ALL;
import static com.example.corona.helper.PlayerConfig.KEY_COVID_COUNTRY;
import static com.example.corona.helper.PlayerConfig.NEWS_BASE_URL;

public class Repository {


    public static Repository instance ;


    private Repository() {

    }



    public static  Repository getInstance (){

        if (instance==null){
            instance = new Repository();
        }


        return instance;
    }



    public Call<ResponseCovidByCountry> getStatus(String country) {
     Api   api = getClient(COMVID_BASE_URL).create(Api.class);

        return api.getStatComvid(HOST_COVID_COUNTRY,KEY_COVID_COUNTRY,country);
    }



    public Call<ResponseCovidAll> getAllStatus() {
      Api  api = getClient(COMVID_ALL_BASE_URL).create(Api.class);

        return api.getAllStatComvid(HOST_COVID_ALL,KEY_COVID_ALL);
    }


    public Call<ResponseNews> getNews(String country ,String q){
      Api  api = getClient(NEWS_BASE_URL).create(Api.class);

        return api.getNews(API_KEY_NEWS,country,q);
    }
}
